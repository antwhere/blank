Blank new tab page or a new tab for the specified url.

✅ open-source: https://bitbucket.org/antwhere/blank/
----------

Update history:
2.4: better support for dark mode
2.3: migrate to Manifest V3
2.2: iframe add referrerpolicy
2.1: support setting background color
2.0: change description text
1.9: make code cross browser
1.8: code optimization
1.7: add a custom title name
1.6: improve user experience
1.5: update description
1.4: improve user experience
1.3: add change address switch
1.2: icon support retina display
1.1: support custom new tab page URL
----------

Privacy Policy:
We do not collect any information from the first version until now and forever!
----------

The initial version began in September 2014.
Feedback E-mail: antwhere@antwhere.com
Official Website: www.antwhere.com