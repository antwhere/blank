﻿document.getElementById("ok").addEventListener("click", function () {
    var input = document.getElementById("url").value.trim();
    var change = document.getElementById("change").checked;
    var title = document.getElementById("title").value.trim();
    var css = document.getElementById("css").value.trim();
    localStorage.setItem("url", input);
    localStorage.setItem("change", change);
    localStorage.setItem("title", title);
    localStorage.setItem("css", css);
    alert("saved!");
    window.close();
});

if (localStorage.getItem("url")) {
    document.getElementById("url").value = localStorage.getItem("url");
}

if (localStorage.getItem("change")) {
    document.getElementById("change").checked = localStorage.getItem("change") == "true" ? true : false;
}

if (localStorage.getItem("title")) {
    document.getElementById("title").value = localStorage.getItem("title");
}

if (localStorage.getItem("css")) {
    document.getElementById("css").value = localStorage.getItem("css");
}